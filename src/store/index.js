import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";

Vue.use(Vuex);

const ignoreMutations = [
  "setProvider"
]

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  reducer: state => ({
    accounts: state.accounts,
    governor: state.governor
  }),
  filter: mutation => (ignoreMutations.indexOf(mutation.type) === -1)
});

export default new Vuex.Store({
  state: {
    provider: null,
    accounts: [],
    governor: {
      name: null,
      address: null
    }
  },
  plugins: [vuexLocal.plugin],
  mutations: {
    setAccounts(state, payload) {
      state.accounts = payload;
    },
    setGovernor(state, payload) {
      state.governor = payload;
    },
    setProvider(state, payload) {
      state.provider = payload;
    }
  },
  actions: {
  },
  modules: {
  }
})
