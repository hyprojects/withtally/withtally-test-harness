import GovernorVotes from "@openzeppelin/contracts/build/contracts/GovernorVotes.json";
import GovernorCountingSimple from "@openzeppelin/contracts/build/contracts/GovernorCountingSimple.json";
import { ethers } from "ethers";

import store from '@/store';

export function getGovernor() {
  const provider = store.state.provider;

  return new ethers.Contract(
    store.state.governor.address,
    GovernorVotes.abi,
    provider);
}

export function getGovernorSimpleCounting() {
  const provider = store.state.provider;

  return new ethers.Contract(
    store.state.governor.address,
    GovernorCountingSimple.abi,
    provider);
}
