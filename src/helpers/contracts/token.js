import ERC20Votes from "@openzeppelin/contracts/build/contracts/ERC20Votes.json";
import IERC20Metadata from "@openzeppelin/contracts/build/contracts/IERC20Metadata.json";
import { getGovernor } from "./governance";
import { ethers } from "ethers";
import { BigNumber } from "bignumber.js";

import store from '@/store';

export const getToken = async () => {
  const governor = getGovernor();
  const provider = store.state.provider;

  const address = await governor.token();

  return new ethers.Contract(
    address,
    ERC20Votes.abi,
    provider);
};

export const getTokenMetadata = async () => {
  const governor = getGovernor();
  const provider = store.state.provider;

  const address = await governor.token();

  return new ethers.Contract(
    address,
    IERC20Metadata.abi,
    provider);
};

export const decimalize = (
  amount,
  decimals,
  fix = Number(decimals.toString())
) => {
  const pow = ethers.utils.parseUnits("1", decimals);
  const formatted = new BigNumber(amount).div(pow.toString()).toFixed(fix);

  return formatted;
};
