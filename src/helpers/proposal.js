import { utils } from "ethers";
import { getGovernor } from "@/helpers/contracts/governance";

export const getProposalEvents = async () => {
  const governance = getGovernor();
  const filters = await governance.filters.ProposalCreated();
  const logs = await governance.queryFilter(filters, 0, "latest");
  const events = logs.map((log) => governance.interface.parseLog(log));

  return events;
};

export const findProposal = async (proposalId) => {
  const events = await getProposalEvents();

  const filteredEvents = events.filter(event => event.args.proposalId.eq(proposalId));

  const filteredEvent = filteredEvents.pop();
  const args = filteredEvent.args;

  return {
    proposalId: args.proposalId.toString(),
    targets: args.targets,
    values: Object.values(args[3]),
    calldatas: args.calldatas,
    description: args.description
  }
};

export const hashProposal = async (proposal) => {
  const governor = getGovernor();
  const descriptionHash = utils.id(proposal.description);

  return await governor.hashProposal(
    proposal.targets,
    proposal.values,
    proposal.calldatas,
    descriptionHash
  );
};
