export const VoteType = {
  Against: "Against",
  For: "For",
  Abstain: "Abstain"
}

Object.freeze(VoteType);

export const VoteTypeIds = {
  0: VoteType.Against,
  1: VoteType.For,
  2: VoteType.Abstain
}

Object.freeze(VoteTypeIds);

export const ProposalState = {
  Pending: "Pending",
  Active: "Active",
  Canceled: "Canceled",
  Defeated: "Defeated",
  Succeeded: "Succeeded",
  Queued: "Queued",
  Expired: "Expired",
  Executed: "Executed"
}

Object.freeze(ProposalState);

export const ProposalStateIds = {
  0: ProposalState.Pending,
  1: ProposalState.Active,
  2: ProposalState.Canceled,
  3: ProposalState.Defeated,
  4: ProposalState.Succeeded,
  5: ProposalState.Queued,
  6: ProposalState.Expired,
  7: ProposalState.Executed

}

Object.freeze(ProposalStateIds);
