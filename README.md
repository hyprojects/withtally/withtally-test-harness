# Withtally Test Harness

WithTally Test Harness provides an interface for interacting with Ethereum-based governance smart contracts that are deployed via a local blockchain instance such as Ganache or Hardhat Node.

## Setup

To set up, start by cloning or downlading this repository. Once cloned, run:

```
npm install
```

This will install all required packages.

## Running the test harness

To launch the Withtally test harness, run:

```
npm run serve
```

Once running, go to your browser and navigate to your instance of Withtally Test Harness.

## Testing your governance contracts

Simply fill in the test harness UI's name and contract address and governance type and click Register. Your contract's details should be read by the test harness and loaded below.

## Using the Withtally Test Governance contracts

Test governance contracts are also available if you want a simple governance example. Governance contracts are avilable at [Governance Test Contracts](https://gitlab.com/hyprojects/withtally/governance-test-contracts).

These contracts are built with Hardhat and so a Hardhat-based environment and basic knowledge of developing smart contracts with Hardhat is required.

## Limitations

This project was inspired by the need to test Open Zeepelin governance contracts without needing to deploy them to a public blockchain such as Rinkeby. Therefore, there are a number of limitations:

### Adding a proposal

When adding a proposal, the contract being executed requires an ABI loaded from a file. Currently, the only files that have been tested are Hardhat generated artifacts (available in the /deployments/localhost directory of your Hardhat build environment) which store the ABI as an object called "abi". Therefore, the uploaded file will need "abi" property explicitly defined at the top level, and will look something like:

```
"address": 0x123,
"abi": ...
```

### Clearing localstorage

Anything that needs to be stored across page refreshes is stored to the browser's localstorage. Therefore, if you shut down your local node and redeploy your contracts, the test harness may not be able to interact with the contracts if they have new addresses. In this case, you can manually clear your local storage using the browser's developer tools. For example, in Firefox, you can right click and select "Inspect". Once the development tools are open, select the "Storage" tab and expand "Local Storage". Find the localhost entry which represents Withtally Test Harness and delete the "vuex" value. Complete by reloading the page. You can now start with a fresh setup without needing to restart the app.

### Proposal state updates

When the state of the proposal changes, you may need to refresh the page to see the updated proposal details. This is a work-in-progress and should be solved in newer versions.

### There is only an OPENZEPPELINGOVERNOR option

Currently, we only use Open Zeppelin simple voting contracts for our development. Therefore, the test harness is calibrated only for these types of contracts. There are plans to support Bravo voting but at this stage the default voting method is GovernorSimpleCounting.
